
  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType('application/json');
  xobj.open('GET', 'questoes/quizTabuleiro.json', true);
  xobj.onreadystatechange = function() {
    if(xobj.readyState === 4 && xobj.status === 200){
      init(xobj.responseText);
    }
  };
  xobj.send();

  var ID_QUESTION = 0;

    function erase() {
      $("#contentQuestion").hide();  
    }

    function  destroyQuestion(){
      $("#contentQuestion").html("");
    }

function init(data) {  
    items = JSON.parse(data);
    console.log(items);


    var callback = function() { $("#gameQuestion")[0].contentWindow.gameQuestionCorrect(); }
    var callbackWrong = function() { $("#gameQuestion")[0].contentWindow.gameQuestionWrong(); }

    function createQuestion(questionIndex, comp){
      var question = items.quiz[questionIndex];
      var content = $("#" + comp);  
      content.show();
      var choices = question.choices;
      var indexChar = 97;
      
      var str = "<form id='quiz_box_id_" + ID_QUESTION + "'><p class='atividade-titulo'>" + question.question + "</p>";  
      for(var i=0;i<choices.length;i++){
        str += "<label class='choiceStyle'><input type='radio' name='choice' value='" + (i + 1) + "'/> " + String.fromCharCode(indexChar++) + ") " + choices[i] + "</label><br/>";
      }
      str += "<input type='button' class='btn btn-sm btn-customized' value='Responder'/></form>";
      
      content.html(str);
      
      $("#quiz_box_id_" + ID_QUESTION + " input[type=button]").click({index: questionIndex}, function(e){       
        var box = $(this).parent();
        var item = $(box).find("input[type=radio]:checked");
        var questionItem = $(item).val();
        if(questionItem == undefined)
          return;
        
        var itemCorrect = quiz[e.data.index].correct;
        var feedback = quiz[e.data.index].feedback[questionItem - 1];
        var questionClass = "questionWrong";
        
        $(box).find("label").removeClass("questionWrong");
        
    if(questionItem == itemCorrect){
         //console.log("correta - " + feedback); 
         questionClass = "questionRight";
         $(box).find('input').attr('disabled', true);
             if(question.callback != undefined)
                   question.callback();
       } 
       else{
         //console.log("errada - " + feedback);
             if(question.callbackWrong != undefined)
                   question.callbackWrong();
       }
        $(item).parent().addClass(questionClass);

        var customModal = $('<div class="custom-modal modal fade"><div class="modal-dialog"><div class="modal-content">' +
        '<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
        '<h4 class="modal-title">Feedback</h4></div>' +
        '<div class="modal-body"><p>' + feedback + '</p></div>' +
        '<div class="modal-footer">' +
        '<button type="button" class="btn btn-customized" data-dismiss="modal">Ok</button>' +
        '</div></div></div></div>');

        $('body').append(customModal);
        $('.custom-modal').modal();
      
        $('.custom-modal').on('hidden.bs.modal', function(){
            console.log("teste");
            $('.custom-modal').remove();
        });

      });
      ID_QUESTION++;
    }
}
    /*var ID_QUESTION = 0;
    var questionIndex = 0;
    var comp = 'contentQuestion';
    var question = items.quiz;
    var content = $("#" + comp);  
    content.show();
    var indexChar = 97;

    for (var i = 0; i <= question.length; i++) {
      question[i]
      var choices = question[i].choices;
      //console.log(choices);
      var str = "<form id='quiz_box_id_" + ID_QUESTION + "'><p class='atividade-titulo'>" + question[i].question + "</p>";  */
    
        /*for(var t=0;t<question[i].choices.length;t++){
          str += "<label class='choiceStyle'><input type='radio' name='choice' value='" + (t + 1) + "'/> " /*+ String.fromCharCode(indexChar++) + ") "+ choices[t] + "</label><br/>";
        }*/
          /*str += "<input type='button' class='btn btn-sm btn-customized' value='Responder'/></form>";
          content.html(str).first(ID_QUESTION);
            var itemCorrect = question[i].correct;
            var feedback = question[i].feedback;
            console.log(itemCorrect);
          $("#quiz_box_id_" + ID_QUESTION + " input[type=button]").click({index: i}, function(e){       
            var box = $(this).parent();
            var item = $(box).find("input[type=radio]:checked");
            var questionItem = $(item).val();
            console.log(box);

            if(questionItem == undefined)
              return;
            $(box).find("label").removeClass("questionWrong");
            
            if(questionItem == itemCorrect){
                 //console.log("correta - " + feedback); 
                var questionClass = "questionRight";
                 $(box).find('input').attr('disabled', true);
                     if(question.callback != undefined)
                           question.callback();
               } 
               else{
                 //console.log("errada - " + feedback);
                     if(question.callbackWrong != undefined)
                           question.callbackWrong();
               }
            $(item).parent().addClass(questionClass);

            var customModal = $('<div class="custom-modal modal fade"><div class="modal-dialog"><div class="modal-content">' +
            '<div class="modal-header"><h4 class="modal-title">Feedback</h4>' +
            '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button></div>' +
            '<div class="modal-body"><p>' + feedback[i] + '</p></div>' +
            '<div class="modal-footer">' +
            '<button type="button" class="btn btn-customized" data-dismiss="modal">Ok</button>' +
            '</div></div></div></div>');

            $('body').append(customModal);
            $('.custom-modal').modal();
          
            $('.custom-modal').on('hidden.bs.modal', function(){
                console.log("teste");
                $('.custom-modal').remove();
            });
          });
          ID_QUESTION++;
      console.log(str);
    }*/

      
      /*var itemCorrect = quiz[e.data.index].correct;
      var feedback = quiz[e.data.index].feedback[questionItem - 1];*/
      /*var questionClass = "questionWrong";
      

    });*/

    
/*gerenciamento das questões*/ 
 

  

  
/*
  function createQuestion(questionIndex, comp){
    var question = items.quiz[questionIndex];*/
    


    
    
    
    
      

      

    
    

    /*return createQuestion();
  }*/



  